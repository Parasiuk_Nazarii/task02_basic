package basic;

public class Main {
    public static void main(String[] args) {
        Interval interval = new Interval();
        interval.setValueFromConsole();
        interval.printOddFromStartToEndAndEvenReverse();
        Fibonacci fibonacci = new Fibonacci();
        fibonacci.set();
        fibonacci.printTheBiggestOddAndEven();
        fibonacci.printPercentOfOddAndEven();
    }
}
